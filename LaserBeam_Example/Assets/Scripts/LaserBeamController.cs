using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserBeamController : MonoBehaviour
{
    private Transform Plane1;
    private Transform Plane2;

    private GameObject HitParticleObj;

    [SerializeField] private float MaxDistance;
    [SerializeField] LayerMask Layer;
    [SerializeField] ParticleSystem HitParticle;
    [SerializeField] private float HitSurfaceCorrection;

    void Start()
    {
        Plane1 = transform.GetChild(0);
        Plane2 = transform.GetChild(1);

        HitParticleObj = Instantiate(HitParticle, this.transform).gameObject;
        HitParticleObj.GetComponent<ParticleSystem>().Stop();
    }

    void FixedUpdate()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, transform.forward, out hit, MaxDistance, Layer))
        {
            Plane1.transform.localScale = new Vector3(hit.distance, 1, 1);
            Plane2.transform.localScale = new Vector3(hit.distance, 1, 1);

            HitParticleObj.transform.position = hit.point + hit.normal * HitSurfaceCorrection;
            HitParticleObj.transform.forward = hit.normal;
            HitParticleObj.GetComponent<ParticleSystem>().Play();
        }
        else
        {
            Plane1.transform.localScale = new Vector3(MaxDistance, 1, 1);
            Plane2.transform.localScale = new Vector3(MaxDistance, 1, 1);

            HitParticleObj.GetComponent<ParticleSystem>().Stop();
            HitParticleObj.transform.position = this.transform.position;
        }
    }
}
